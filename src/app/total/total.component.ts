import { Component, Input, OnInit } from '@angular/core';
import { Status } from 'src/interface/status';
import { Week } from 'src/interface/week';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {

  changeColour = {
    
  }

  @Input() totalNumber;
  @Input() cardTotal : Status[];

  constructor() { }

  ngOnInit(): void {
  }

}
