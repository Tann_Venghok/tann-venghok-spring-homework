import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TotalComponent } from './total/total.component';
import { WeekRowComponent } from './week-row/week-row.component';
import { BranchComponent } from './branch/branch.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TotalFormatPipe } from './total-format.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TotalComponent,
    WeekRowComponent,
    BranchComponent,
    TotalFormatPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
