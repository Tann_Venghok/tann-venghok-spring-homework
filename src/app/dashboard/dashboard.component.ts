import { Component, OnInit } from '@angular/core';
import { Status } from 'src/interface/status';
import { Week } from 'src/interface/week';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  numberOfWeek: number = 1;

  cardDatas: Status[] = [
    { used: Math.floor(Math.random() * 30 + 1), available: 30 },
    { used: Math.floor(Math.random() * 20 + 1), available: 20 },
    { used: Math.floor(Math.random() * 25 + 1), available: 25 },
  ];

  constructor() {}

  ngOnInit(): void {}

  getTotalData = function () {
    return {
      regular: { used: Math.floor(Math.random() * 31), available: 30 },
      diesel: { used: Math.floor(Math.random() * 20 + 1), available: 20 },
      premium: { used: Math.floor(Math.random() * 25 + 1), available: 25 },
    };
  };

  getWeeklyData = function () {
    return {
      name: 'Week',
      regular: { used: Math.floor(Math.random() * 31), available: 30 },
      diesel: { used: Math.floor(Math.random() * 20 + 1), available: 20 },
      premium: { used: Math.floor(Math.random() * 25 + 1), available: 25 },
    };
  };
}
