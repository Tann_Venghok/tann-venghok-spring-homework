import { Component, Input, OnInit } from '@angular/core';
import { Week } from 'src/interface/week';

@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit {

  @Input() i;
  @Input() week: Week;

  constructor() { }

  ngOnInit(): void {
  }

}
